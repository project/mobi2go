(function ($) {
  Drupal.behaviors.mobi2go = {
    attach: function(context, settings) {
      var d = document,
      jqv = parseFloat(window.jQuery.fn.jquery.substring(0,3), 10);
      jq = window.jQuery && jqv >= 1.7,
      qs = window.location.search.substring(1),
      re = '=(.*?)(?:;|$)',
      c = d.cookie.match('MOBI2GO_SESSIONID' + re);

      src  = 'https://www.mobi2go.com/store/embed/';
      src += settings.mobi2go.site + '.js';
      src += qs + (jq?'&no_jquery':'') + (c?'&s='+c[1]:'');

      $.getScript(src, function() {
        Mobi2Go.load(settings.mobi2go.container, function() {});
      });
    }
  };
}) (jQuery);
