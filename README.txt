INTRODUCTION
------------

The Mobi2Go module makes it to include your online ordering form
into your existing drupal without the need for horrible iframes.

INSTALLATION
------------

If you do not already have a Mobi2Go account sign up for one
(http://mobi2go.com/signup).

 * Grab a copy of the module
 * Place it in sites/all/modules directory (make sure it's named mobi2go)
 * Activate the module
 * Configure the module, add your sitename (You can get it from the console)
 * Add the Mobi2Go Block to the content region and set it to only display on
   your ordering page

TROUBLESHOOTING
---------------

 * If your getting a 500 service timed out error. You will need to add your
   domain name to the external domain settings in the Mobi2Go console.
