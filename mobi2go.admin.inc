<?php
/**
 * @file
 * Mobi2Go Admin form.
 *
 * Admin config form for Mobi2Go.
 */

/**
 * Admin form function.
 *
 * Settings form fo mobi2go.
 */
function mobi2go_admin() {
  $form = array();

  $form['mobi2go_site'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Name'),
    '#default_value' => variable_get('mobi2go_site', ''),
    '#description' => t('Site name from console.'),
    '#required' => TRUE,
  );

  $form['mobi2go_container'] = array(
    '#type' => 'textfield',
    '#title' => t('Container'),
    '#default_value' => variable_get('mobi2go_container', 'mobi2go-ordering'),
    '#description' => t('The div id to embed mobi2go into.'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
